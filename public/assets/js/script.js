(function($,sr){

  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function (func, threshold, execAsap) {
      var timeout;

      return function debounced () {
          var obj = this, args = arguments;
          function delayed () {
              if (!execAsap)
                  func.apply(obj, args);
              timeout = null;
          };

          if (timeout)
              clearTimeout(timeout);
          else if (execAsap)
              func.apply(obj, args);

          timeout = setTimeout(delayed, threshold || 100);
      };
  }
  // smartresize
  jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');

var $ = jQuery;


(function(){


  ///////////////////////////////
  // Set Home Slideshow Height
  ///////////////////////////////

  function setHomeBannerHeight() {
    var windowHeight = jQuery(window).height();
    jQuery('#header').height(windowHeight);
  }

  ///////////////////////////////
  // Center Home Slideshow Text
  ///////////////////////////////

  function centerHomeBannerText() {
      var bannerText = jQuery('#header > .center');
      var bannerTextTop = (jQuery('#header').actual('height')/2) - (jQuery('#header > .center').actual('height')/2) - 40;
      bannerText.css('padding-top', bannerTextTop+'px');
      bannerText.show();
  }

  function setHeaderBackground() {
    var scrollTop = jQuery(window).scrollTop(); // our current vertical position from the top

    if (scrollTop > 300 || jQuery(window).width() < 700) {
      jQuery('#header .top').addClass('solid');
    } else {
      jQuery('#header .top').removeClass('solid');
    }
  }




  ///////////////////////////////
  // Initialize
  ///////////////////////////////

  jQuery.noConflict();
  setHomeBannerHeight();
  centerHomeBannerText();

  //Resize events
  jQuery(window).smartresize(function(){
    setHomeBannerHeight();
    centerHomeBannerText();
  });

})();


  ///////////////////////////////
  // Smooth Scroll
  ///////////////////////////////


smoothScroll.init();




  ///////////////////////////////
  // Animate Css
  ///////////////////////////////
var $ = jQuery;

function animationHover(element, animation){
    element = $(element);
    element.hover(
        function() {
            element.addClass('animated ' + animation);
        },
        function(){
            //wait for animation to finish before removing classes
            window.setTimeout( function(){
                element.removeClass('animated ' + animation);
            }, 2000);
        });
}

$(document).ready(function(){
    $('#scrollToContent').each(function() {
        animationHover(this, 'pulse');
    });
});



  ///////////////////////////////
  // Header Fixed
  ///////////////////////////////



var menu = $('#navigation');
var origOffsetY = menu.offset().top;

function scroll() {
   if ($(window).scrollTop() >= origOffsetY) {
       $('#navigation').addClass('nav-wrap');
       $('#about').addClass('exp');
   } else {
       $('#navigation').removeClass('nav-wrap');
       $('#about').removeClass('exp');
   }



}

 document.onscroll = scroll;


 $(document).ready(function() {
     $("#LoadingAlert").hide();
      $("#DangerAlert").hide();
      $("#SuccessAlert").hide();

   $("form#messageForm").submit(function(event) {
        event.preventDefault();
        // get the form data
        // there are many ways to get this data using jQuery (you can use the class or id also)
        var formData = {
            'name'              : $('input[name=name]').val(),
            'email'             : $('input[name=email]').val(),
            'message'           : $('textarea[name=message]').val()
        };

        $("form#messageForm").hide();
        $("#LoadingAlert").fadeIn("slow");

        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : '/send', // the url where we want to POST
            data        : formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        }).done(function(data) {
          // log data to the console so we can see
          console.log(data);
          $("#LoadingAlert").hide();
          if (data.success) {
            $("#SuccessAlert").text(data.successText);
            $("#SuccessAlert").fadeIn("slow");
          } else {
            $("#DangerAlert").text(data.successText);
            $("#DangerAlert").fadeIn("slow");
          }
          // here we will handle errors and validation messages
        }).fail(function(data) {
          // show any errors
          // best to remove for production
          //console.log(data);
          $("#LoadingAlert").hide();
          $("#DangerAlert").text("An error occured and have been sent. Please try again later.");
          $("#DangerAlert").fadeIn("slow");
        });
    });

});




  ///////////////////////////////
  // google map
  ///////////////////////////////

function initialize()
{
var mapProp = {
  center:new google.maps.LatLng(50.6309806,3.0523529),
  zoom:16,
  mapTypeId:google.maps.MapTypeId.SATELLITE,
  disableDefaultUI: true,
  scrollwheel: false
  };
var map=new google.maps.Map(document.getElementById("googleMap")
  ,mapProp);
}

google.maps.event.addDomListener(window, 'load', initialize);
