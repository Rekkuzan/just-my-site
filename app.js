const express = require('express'),
      i18n = require("i18n"),
      bodyParser = require('body-parser'),
      sendmail = require('sendmail')();

i18n.configure({
    locales:['fr', 'en'],
    queryParameter: 'lang',
    defaultLocale: 'en',
    register: global,
    directory: __dirname + '/locales'
});

var RateLimit = require('express-rate-limit');

const app = express();

app.use(i18n.init);
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.set('port', (process.env.PORT || 3000));
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');

app.get('/:lang', function(req, res) {
   i18n.setLocale(req, req.params.lang);
   res.render('pages/index', {'lang' : req.params.lang});
 });


app.get('/', function (req, res) {
  i18n.setLocale(req, "en");
  i18n.setLocale(res, "en");
  res.render('pages/index', {'lang' : res.getLocale()});
});

var sendMessageLimit = new RateLimit({
  windowMs: 60*30*1000, // 30 minutes
  delayAfter: 3, // begin slowing down responses after the first request
  delayMs: 3*1000, // slow down subsequent responses by 3 seconds per request
  max: 10, // start blocking after 10 requests
  message: "Too many request"
});

app.post('/send', sendMessageLimit, function (req, res) {
    console.log(req.body.name);
    console.log(req.body.email);
    console.log(req.body.message);

    var name = req.body.name;
    var email = req.body.email;
    var message = req.body.message;

    sendmail({
      from: 'no-reply@rekkuzan.com',
      to: 'noel.pierre.26@gmail.com',
      subject: 'Message from your website [' + name  + ']',
      html: 'This is the message you got : '  + message + " (email : " + email + " name : " + name + ")",
    }, function(err, reply) {
        console.log(err && err.stack);
        console.dir(reply);
        var jsonRes;
        if (err) {
          jsonRes = {success : false, successText: res.__('ERROR_FORM')};
        } else {
          jsonRes = {success : true, successText: res.__('SUCCESS_FORM')};
        }
      res.send(JSON.stringify(jsonRes));
    });
});

app.listen(app.get('port'), function () {
  console.log('Server launched')
})
